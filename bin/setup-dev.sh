#!/usr/bin/env bash

composer install --no-interaction --optimize-autoloader --prefer-dist

php app/console doctrine:database:create --if-not-exists
php app/console doctrine:migration:migrate
php app/console doctrine:fixtures:load -n --fixtures=src/AppBundle/DataFixtures/
php app/console assets:install
php app/console app:fix:context

php app/console server:start --force