#!/usr/bin/env bash

cp app/phpunit.xml.dist app/phpunit.xml

php app/console doctrine:schema:drop --force -e test
php app/console doctrine:schema:update --force -e test
php app/console doctrine:fixtures:load -n -e test
php app/console app:fix:context