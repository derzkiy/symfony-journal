#!/usr/bin/env bash

php app/console doctrine:migrations:migrate -n -e prod
php app/console assets:install -e prod
php app/console app:fix:context -e prod