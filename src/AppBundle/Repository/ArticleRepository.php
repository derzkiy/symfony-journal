<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 13.05.18
 * Time: 17:12
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class ArticleRepository extends EntityRepository
{
    public function findAllQueryBuilder()
    {
        $queryBuilder = $this->createQueryBuilder('article');
        $queryBuilder
            ->where('article.published = :published')
            ->andWhere('article.publicationStartDate <= :date')
            ->setParameters([
                'published' => true,
                'date' => new \DateTime(),
            ])
            ->orderBy('article.publicationStartDate', 'DESC')
        ;

        return $queryBuilder;
    }
}