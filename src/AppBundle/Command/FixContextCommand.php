<?php
/**
 * symfony-journal - FixContextCommand.php
 *
 * Initial version by: Andrei Novikov <andrei.novikov@isobar.ru>
 * Initial version created on: 14.05.18
 */

namespace AppBundle\Command;


use Application\Sonata\ClassificationBundle\Entity\Context;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixContextCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('app:fix:context')
            ->setDescription('Create or enable default context')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Start fixing context');
        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        $contextRepository = $entityManager->getRepository(Context::class);

        $context = $contextRepository->findOneBy(['id' => 'default']);

        if (!$context) {
            $context = new Context();
            $context->setName('Default');
            $entityManager->persist($context);
        }

        $context->setEnabled(true);
        $entityManager->flush();

        $output->writeln('Done!');
    }
}