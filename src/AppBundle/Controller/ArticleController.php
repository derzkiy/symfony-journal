<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 13.05.18
 * Time: 18:07
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Article;
use AppBundle\Form\ArticleType;
use AppBundle\Model\DataProviderFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ArticleController
 */
class ArticleController extends Controller
{
    /**
     * @param int $page
     *
     * @return Response
     * @Route("/{page}", name="homepage", defaults={"page": 1}, requirements={"page" : "\d+"})
     */
    public function indexAction($page)
    {
        $queryBuilder = $this->getDoctrine()->getRepository(Article::class)->findAllQueryBuilder();

        $count = DataProviderFactory::getTotalCount(clone $queryBuilder);
        $items = DataProviderFactory::createCollection($queryBuilder, $page);

        if (!$items) {
            throw $this->createNotFoundException('Page not found');
        }

        return $this->render('article/index.html.twig', [
            'items' => $items,
            'totalCount' => $count,
            'page' => $page,
            'maxPages' => ceil($count / DataProviderFactory::ITEMS_PER_PAGE),
        ]);
    }

    /**
     * @param Article $article
     * @Route("/article/{slug}")
     * @ParamConverter("article", options={"mapping": {"slug": "slug"}})
     */
    public function viewAction(Article $article)
    {
        return $this->render('article/view.html.twig', array('article' => $article));
    }
}