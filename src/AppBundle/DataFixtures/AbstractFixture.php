<?php

namespace AppBundle\DataFixtures;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture as BaseFixture;
use Symfony\Component\PropertyAccess\PropertyAccess;

abstract class AbstractFixture extends BaseFixture implements OrderedFixtureInterface
{
    static protected $propertyAccessor;

    public function load(ObjectManager $manager)
    {
        $accessor = $this->getPropertyAccessor();
        foreach ($this->getData() as $key => $entityFields) {
            $entity = $this->createEntity();

            foreach ($entityFields as $field => $value) {
                $accessor->setValue($entity, $field, $value);
            }

            $manager->persist($entity);
            $manager->flush();

            $this->referenceRepository->setReference($this->getName().'.'.$key, $entity);
        }
    }

    abstract public function getData();

    abstract public function getName();

    abstract public function createEntity();

    public function getPropertyAccessor() {
        if (!self::$propertyAccessor) {
            self::$propertyAccessor = PropertyAccess::createPropertyAccessor();
        }

        return self::$propertyAccessor;
    }
}