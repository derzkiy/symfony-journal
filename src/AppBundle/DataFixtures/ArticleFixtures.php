<?php
/**
 * symfony-journal - ArticleFixtures.php
 *
 * Initial version by: Andrei Novikov <andrei.novikov@isobar.ru>
 * Initial version created on: 14.05.18
 */

namespace AppBundle\DataFixtures;


use AppBundle\Entity\Article;

class ArticleFixtures extends AbstractFixture
{
    public function getData()
    {
        return [
            'a1' => [
                'title' => 'Title 1',
                'content' => '<p>Content</p>',
                'published' => true,
                'publicationStartDate' => new \DateTime(),
                'tags' => [
                    $this->getReference('tag.t3'),
                    $this->getReference('tag.t1')
                ]
            ],
            'a2' => [
                'title' => 'Title 2',
                'content' => '<p>Content Content</p>',
                'published' => false,
                'publicationStartDate' => new \DateTime(),
                'tags' => [
                    $this->getReference('tag.t2'),
                ]
            ],
            'a3' => [
                'title' => 'Title 3',
                'content' => '<p>Content Content Content</p>',
                'published' => true,
                'publicationStartDate' => new \DateTime(),
                'tags' => [
                    $this->getReference('tag.t2'),
                    $this->getReference('tag.t4')
                ]
            ],
        ];
    }

    public function getName()
    {
        return 'article';
    }

    public function createEntity()
    {
        return new Article();
    }

    public function getOrder()
    {
        return 30;
    }
}