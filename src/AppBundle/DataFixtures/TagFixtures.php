<?php
/**
 * symfony-journal - TagFixtures.php
 *
 * Initial version by: Andrei Novikov <andrei.novikov@isobar.ru>
 * Initial version created on: 14.05.18
 */

namespace AppBundle\DataFixtures;


use Application\Sonata\ClassificationBundle\Entity\Tag;

class TagFixtures extends AbstractFixture
{
    public function getData()
    {
        return [
            't1' => [
                'context' => $this->getReference('context.c1'),
                'name' => 'Tag 1',
                'enabled' => true,
            ],
            't2' => [
                'context' => $this->getReference('context.c1'),
                'name' => 'Tag 2',
                'enabled' => false,
            ],
            't3' => [
                'context' => $this->getReference('context.c1'),
                'name' => 'Tag 3',
                'enabled' => true,
            ],
            't4' => [
                'context' => $this->getReference('context.c2'),
                'name' => 'Custom tag',
                'enabled' => true,
            ],
        ];
    }

    public function getName()
    {
        return 'tag';
    }

    public function createEntity()
    {
        return new Tag();
    }

    public function getOrder()
    {
        return 20;
    }
}