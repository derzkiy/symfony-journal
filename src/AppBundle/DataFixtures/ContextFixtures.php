<?php
/**
 * symfony-journal - ContextFixtures.php
 *
 * Initial version by: Andrei Novikov <andrei.novikov@isobar.ru>
 * Initial version created on: 14.05.18
 */

namespace AppBundle\DataFixtures;


use Application\Sonata\ClassificationBundle\Entity\Context;

class ContextFixtures extends AbstractFixture
{
    public function getData()
    {
        return [
            'c1' => [
                'id' => 'default',
                'name' => 'Default',
                'enabled' => true
            ],
            'c2' => [
                'id' => 'custom',
                'name' => 'Custom',
                'enabled' => false
            ],
        ];
    }

    public function getName()
    {
        return 'context';
    }

    public function createEntity()
    {
        return new Context();
    }

    public function getOrder()
    {
        return 10;
    }

}