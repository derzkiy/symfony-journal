<?php
/**
 * symfony-journal - DataProvider.php
 *
 * Initial version by: Andrei Novikov <andrei.novikov@isobar.ru>
 * Initial version created on: 14.05.18
 */

namespace AppBundle\Model;


use Doctrine\ORM\QueryBuilder;

class DataProviderFactory
{
    const ITEMS_PER_PAGE = 10;

    public static function getTotalCount(QueryBuilder $queryBuilder)
    {
        $count = $queryBuilder
            ->select($queryBuilder->expr()->count(self::getAlias($queryBuilder) . '.id'))
            ->getQuery()
            ->getSingleScalarResult();

        return $count;
    }

    public static function createCollection(QueryBuilder $queryBuilder, $page)
    {
        $limit = self::ITEMS_PER_PAGE;
        $offset = $limit * ($page - 1);

        $queryBuilder
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;

        return $queryBuilder->getQuery()->getResult();
    }

    private static function getAlias(QueryBuilder $queryBuilder)
    {
        $aliaces = $queryBuilder->getRootAliases();
        return array_shift($aliaces);
    }
}