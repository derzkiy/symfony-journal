<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 13.05.18
 * Time: 23:19
 */

namespace AppBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait PublicationTrait
 */
trait PublicationTrait
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $publicationStartDate;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $published = false;

    /**
     * @return \DateTime
     */
    public function getPublicationStartDate()
    {
        return $this->publicationStartDate;
    }

    /**
     * @param \DateTime $publicationStartDate
     * @return self
     */
    public function setPublicationStartDate($publicationStartDate)
    {
        $this->publicationStartDate = $publicationStartDate;

        return $this;
    }

    public function isPublished($considerDates = false)
    {
        if (!$considerDates) {
            return $this->published;
        }

        $now = new \DateTime();
        return $this->published && ($this->publicationStartDate > $now);
    }

    /**
     * @param boolean $published
     * @return self
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }
}